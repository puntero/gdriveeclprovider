﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using GDriveHelper;
using GdriveECLProvider;
using Google.Apis.Drive.v2.Data;
using Tridion.ExternalContentLibrary.V2;

namespace GdriveProvider
{
    public class GDriveContext : IContentLibraryContext
    {
        public void Dispose()
        {
            
        }

        public string Dispatch(string command, string payloadVersion, string payload, out string responseVersion)
        {
            throw new NotImplementedException();
        }

        public bool CanGetUploadMultimediaItemsUrl(int publicationId)
        {
            return false;
        }

        public bool CanSearch(int publicationId)
        {
            return false;
        }

        public IList<IContentLibraryListItem> FindItem(IEclUri eclUri)
        {
            return null;
        }

        public IFolderContent GetFolderContent(IEclUri parentFolderUri, int pageIndex, EclItemTypes itemTypes)
        {
            bool canPaginate = false;
            bool canSearch = false;

            List<IContentLibraryListItem> result = new List<IContentLibraryListItem>();

            foreach (File folder in GDrive.GDriveConnection.GetRootList())
            {
                result.Add(new GDriveFolder(parentFolderUri.PublicationId, folder.Id,folder.Title));
            }

            return GDrive.HostServices.CreateFolderContent(parentFolderUri, result, false, canSearch);
        }

        public IContentLibraryItem GetItem(IEclUri eclUri)
        {
            return null;
        }

        public IList<IContentLibraryItem> GetItems(IList<IEclUri> eclUris)
        {
            return null;
        }

        public byte[] GetThumbnailImage(IEclUri eclUri, int maxWidth, int maxHeight)
        {
            return null;
        }

        public string GetUploadMultimediaItemsUrl(IEclUri parentFolderUri)
        {
            return null;
        }

        public string GetViewItemUrl(IEclUri eclUri)
        {
            return null;
        }

        public IFolderContent Search(IEclUri contextUri, string searchTerm, int pageIndex, int numberOfItems)
        {
            return null;
        }

        public void StubComponentCreated(IEclUri eclUri, string tcmUri)
        {
            
        }

        public void StubComponentDeleted(IEclUri eclUri, string tcmUri)
        {
            
        }

        public string IconIdentifier { get; private set; }
    }
}