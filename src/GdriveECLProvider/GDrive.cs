﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using GDriveHelper;
using Tridion.ExternalContentLibrary.V2;

namespace GdriveProvider
{
    [AddIn("GDriveProvider", Version="1.0.0.0")]
    public class GDrive : IContentLibrary
    {
        internal static GDriveConnection GDriveConnection { get; private set;}
        internal static string MountPointId { get; private set; }
        internal static IHostServices HostServices { get; private set; }

        
        public void Dispose()
        {
            throw new NotImplementedException();
            
        }

        public IContentLibraryContext CreateContext(IEclSession session)
        {
            return new GDriveContext();
            
        }

        public byte[] GetIconImage(string theme, string iconIdentifier, int iconSize)
        {
            return null;
        }

        public void Initialize(string mountPointId, string configurationXmlElement, IHostServices hostServices)
        {
            //TODO: We should get these constants probably from configuration
            String CLIENT_ID = "";
            String CLIENT_SECRET = "";

            GDriveConnection = new GDriveConnection();
            GDriveConnection.Authenticate(CLIENT_ID,CLIENT_SECRET);
            
            
            HostServices = hostServices;
            MountPointId = mountPointId;

        }

        public IList<IDisplayType> DisplayTypes
        {
            get
            {
                // These strings control the ItemType description displayed in the SDL Tridion
                // user interface.
                return new[]
                    {
                        HostServices.CreateDisplayType("gdf", "Google Drive Folder", EclItemTypes.Folder),
                        HostServices.CreateDisplayType("gdi", "Google Drive Item", EclItemTypes.File),
                    };
            }
        }
    }
}
