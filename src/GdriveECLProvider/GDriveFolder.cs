﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GdriveProvider;
using Tridion.ExternalContentLibrary.V2;

namespace GdriveECLProvider
{
    public class GDriveFolder :IContentLibraryListItem
    {
        private readonly IEclUri _id;
        private readonly string _name;

        public GDriveFolder(int publicationId, string id, string name)
        {
            _id = GDrive.HostServices.CreateEclUri(publicationId, GDrive.MountPointId, id, "gdf", EclItemTypes.Folder);
            _name = name;

        }


        public string Dispatch(string command, string payloadVersion, string payload, out string responseVersion)
        {
            throw new NotImplementedException();
        }

        public bool CanGetUploadMultimediaItemsUrl { get; private set; }

        public bool CanSearch { get; private set; }

        public string DisplayTypeId {
            get { return "gdf"; }
        }
        public string IconIdentifier
        {
            get { return null; }
        }

        public IEclUri Id
        {
            get { return _id; }
        }

        public bool IsThumbnailAvailable
        {
            get { return false; }
        }
        public DateTime? Modified
        {
            get { return null; }
        }


        public string ThumbnailETag
        {
            get { return null; }
        }


        public string Title
        {
            get { return _name; }
            set { throw new NotSupportedException(); }
        }
    }
}
