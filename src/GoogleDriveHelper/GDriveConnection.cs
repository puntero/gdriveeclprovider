﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;
using Google.Apis.Util;
using Google.Apis.Services;


namespace GDriveHelper
{
    public class GDriveConnection
    {
        private DriveService _service;

        public void Authenticate(string CLIENT_ID, string CLIENT_SECRET)
        {
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description, CLIENT_ID, CLIENT_SECRET);
            var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);
            _service = new DriveService(new BaseClientService.Initializer()
            {
                Authenticator = auth
            });
        }

        private static IAuthorizationState GetAuthorization(NativeApplicationClient arg)
        {
            // Get the auth URL:
            IAuthorizationState state = new AuthorizationState(new[] { DriveService.Scopes.Drive.GetStringValue() });
            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
            Uri authUri = arg.RequestUserAuthorization(state);

            // Request authorization from the user (by opening a browser window):
            Process.Start(authUri.ToString());
            Console.Write("  Authorization Code: ");
            string authCode = Console.ReadLine();
            Console.WriteLine();

            // Retrieve the access token by using the authorization code:
            return arg.ProcessUserAuthorization(authCode, state);
        }


        public File UploadFile(String title, string description, string mimeType, string filePath)
        {
            File body = new File();

            body.Title = title;
            body.Description = description;
            body.MimeType = mimeType;

            byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

            FilesResource.InsertMediaUpload request = _service.Files.Insert(body, stream, mimeType);
            request.Upload();

            return request.ResponseBody;

        }

        public List<File> GetRootList()
        {
            return GetListByFolder("");
        }

        public List<File> GetListByFolder(String parentId)
        {

            List<File> result = new List<File>();
            FilesResource.ListRequest request = _service.Files.List();

            if(parentId != "")
                request.Q = String.Format("'{0}' in parents", parentId);

            do
            {
                try
                {
                    FileList files = request.Fetch();

                    result.AddRange(files.Items);
                    request.PageToken = files.NextPageToken;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                    request.PageToken = null;
                }
            } while (!String.IsNullOrEmpty(request.PageToken));
            return result;

        }



    }
}
